//
//  AppDetailViewController.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 20.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit

class AppDetailViewController: UIViewController {
    
    public var app: iTunesApp!
    
    private let imageDownloader = ImageDownloader()
    private var imageView: UIImageView!
    private var throbber: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.downloadImage()
    }
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        self.navigationItem.largeTitleDisplayMode = .never
        self.addImageView()
        self.addImageViewThrobber()
        self.setupConstraints()
    }
    
    private func addImageView() {
        self.imageView = UIImageView(frame: .zero)
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.imageView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        self.imageView.layer.cornerRadius = 10.0
        self.imageView.layer.masksToBounds = true
        self.view.addSubview(self.imageView)
    }
    
    private func addImageViewThrobber() {
        self.throbber = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.imageView.addSubview(self.throbber)
    }
    
    private func setupConstraints() {
        self.imageView.widthAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.imageView.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        self.imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        
        self.throbber.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor).isActive = true
        self.throbber.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor).isActive = true
    }
    
    private func downloadImage() {
        guard let url = self.app.iconUrl else { return }
        self.throbber.startAnimating()
        self.imageDownloader.getImage(fromUrl: url) { (image, error) in
            self.throbber.stopAnimating()
            guard let image = image else { return }
            self.imageView.image = image
        }
    }
}
