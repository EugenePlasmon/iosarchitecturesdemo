//
//  ViewController.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 14.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    enum SearchType: Int {
        case apps = 0
        case songs = 1
    }
    
    var segmentedControl: UISegmentedControl!
    var searchBar: UISearchBar!
    var tableView: UITableView!
    var emptyResultView: UIView!
    var emptyResultLabel: UILabel!
    
    var searchType: SearchType = .apps
    let searchManager = iTunesSearchManager()
    var searchResults = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.throbber(show: false)
    }
    
    private func configureUI() {
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.addSegmentedControl()
        self.addSearchBar()
        self.addTableView()
        self.addEmptyResultView()
        self.setupConstraints()
    }
    
    private func addSegmentedControl() {
        self.segmentedControl = UISegmentedControl(items: ["Apps", "Songs"])
        self.segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        self.segmentedControl.tintColor = UIColor.iviVarna
        self.segmentedControl.selectedSegmentIndex = self.searchType.rawValue
        self.segmentedControl.addTarget(self, action: #selector(segmentedControlDidChangeValue), for: .valueChanged)
        self.view.addSubview(self.segmentedControl)
    }
    
    private func addSearchBar() {
        self.searchBar = UISearchBar(frame: .zero)
        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
        self.searchBar.delegate = self
        self.searchBar.searchBarStyle = .minimal
        self.view.addSubview(self.searchBar)
    }
    
    private func addTableView() {
        self.tableView = UITableView(frame: .zero)
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
    }
    
    private func addEmptyResultView() {
        self.emptyResultView = UIView(frame: .zero)
        self.emptyResultView.translatesAutoresizingMaskIntoConstraints = false
        self.emptyResultView.backgroundColor = .white
        self.emptyResultView.isHidden = true
        
        self.emptyResultLabel = UILabel(frame: .zero)
        self.emptyResultLabel.translatesAutoresizingMaskIntoConstraints = false
        self.emptyResultLabel.text = "Nothing was found"
        self.emptyResultLabel.textColor = UIColor.darkGray
        self.emptyResultLabel.textAlignment = .center
        self.emptyResultLabel.font = UIFont.systemFont(ofSize: 12.0)
        
        self.view.addSubview(self.emptyResultView)
        self.emptyResultView.addSubview(self.emptyResultLabel)
    }
    
    private func setupConstraints() {
        let safeArea = view.safeAreaLayoutGuide
        
        let segmentedControlMargin: CGFloat = 28.0;
        let segmentedControlTopOffset: CGFloat = 8.0
        self.segmentedControl.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: segmentedControlTopOffset).isActive = true
        self.segmentedControl.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: segmentedControlMargin).isActive = true
        self.segmentedControl.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -segmentedControlMargin).isActive = true
        self.segmentedControl.heightAnchor.constraint(equalToConstant: segmentedControl.frame.height)
        
        self.searchBar.topAnchor.constraint(equalTo: self.segmentedControl.bottomAnchor).isActive = true
        self.searchBar.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        self.searchBar.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        self.searchBar.heightAnchor.constraint(equalToConstant: searchBar.frame.height)
        
        self.tableView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor).isActive = true
        self.tableView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        self.tableView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        self.tableView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
        
        self.emptyResultView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor).isActive = true
        self.emptyResultView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor).isActive = true
        self.emptyResultView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor).isActive = true
        self.emptyResultView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
        
        self.emptyResultLabel.topAnchor.constraint(equalTo: self.emptyResultView.topAnchor, constant: 12.0).isActive = true
        self.emptyResultLabel.leadingAnchor.constraint(equalTo: self.emptyResultView.leadingAnchor).isActive = true
        self.emptyResultLabel.trailingAnchor.constraint(equalTo: self.emptyResultView.trailingAnchor).isActive = true
    }
    
    private func throbber(show: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = show
    }
    
    private func showError(error: Error) {
        let ac = UIAlertController(title: "Error", message: "\(error.localizedDescription)", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        ac.addAction(actionOk)
        self.present(ac, animated: true, completion: nil)
    }
    
    private func showNoResults() {
        self.emptyResultView.isHidden = false
    }
    
    private func hideNoResults() {
        self.emptyResultView.isHidden = true
    }
    
    @objc private func segmentedControlDidChangeValue(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.searchType = .apps
        case 1:
            self.searchType = .songs
        default:
            break
        }
    }
}

//MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        
        let reuseId = "reuseId"
        var cell = tableView.dequeueReusableCell(withIdentifier: reuseId)
        if cell == nil {
            cell = UITableViewCell(style: .value1, reuseIdentifier: reuseId)
        }
        
        if let app = searchResult as? iTunesApp {
            cell?.textLabel?.text = app.appName ?? ""
            cell?.detailTextLabel?.text = app.averageRating != nil ? "\(app.averageRating!)" : ""
        } else if let song = searchResult as? iTunesSong {
            cell?.textLabel?.text = song.trackName ?? ""
            cell?.detailTextLabel?.text = song.artistName ?? ""
        }
        
        return cell!
    }
}

//MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let searchResult = searchResults[indexPath.row]
        
        if let app = searchResult as? iTunesApp {
            let appDetaillViewController = AppDetailViewController()
            appDetaillViewController.app = app
            navigationController?.pushViewController(appDetaillViewController, animated: true)
            return
        }
        
        if let song = searchResult as? iTunesSong {
            let songDetaillViewController = SongDetailViewController()
            songDetaillViewController.song = song
            navigationController?.pushViewController(songDetaillViewController, animated: true)
            return
        }
    }
}

//MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let query = searchBar.text else {
            searchBar.resignFirstResponder()
            return
        }
        if query.count == 0 {
            searchBar.resignFirstResponder()
            return
        }
        
        self.throbber(show: true)
        self.searchResults = []
        self.tableView.reloadData()
        
        let completionHandler: ([AnyObject]?, Error?) -> () = { [weak self] (results, error) in
            guard error == nil else {
                self?.showError(error: error!)
                return
            }
            guard let results = results else {
                self?.showNoResults()
                return
            }
            if results.count == 0 {
                self?.showNoResults()
            } else {
                self?.hideNoResults()
            }
            self?.throbber(show: false)
            self?.searchResults = results
            
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            
            self?.searchBar.resignFirstResponder()
        }
        
        switch self.searchType {
        case .apps:
            self.searchManager.getApps(forQuery: query, completion: { (apps, error) in
                completionHandler(apps, error)
            })
        case .songs:
            self.searchManager.getSongs(forQuery: query, completion: { (songs, error) in
                completionHandler(songs, error)
            })
        }
    }
}
