//
//  SongDetailViewController.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 21.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit

class SongDetailViewController: UIViewController {
    
    public var song: iTunesSong!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
    }
}
