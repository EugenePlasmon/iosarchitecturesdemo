//
//  iTunesAppsParser.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import Foundation


class iTunesAppsParser: NSObject {
    
    private typealias AppDictionary = [String: Any]
    
    private let kAppName = "trackName"
    private let kAppUrl = "artistViewUrl"
    private let kCompany = "sellerName"
    private let kCompanyUrl = "sellerUrl"
    private let kAppDesctiption = "description"
    private let kAverageRating = "averageUserRating"
    private let kAverageRatingForCurrentVersion = "averageUserRatingForCurrentVersion"
    private let kSize = "fileSizeBytes"
    private let kImageUrl = "artworkUrl512"
    private let kScreenshotsArray = "screenshotUrls"
    
    public func parseResponse(_ response: Response) -> [iTunesApp] {
        let results = response["results"] as! [AppDictionary]
        
        var apps = [iTunesApp]()
        
        for appJson in results {
            let app = parseAppJson(appJson)
            apps.append(app)
        }
        
        return apps
    }
    
    private func parseAppJson(_ appJson: AppDictionary) -> iTunesApp {
        let appName = appJson[kAppName] as? String
        let appUrl = appJson[kAppUrl] as? String
        let company = appJson[kCompany] as? String
        let companyUrl = appJson[kCompanyUrl] as? String
        let appDescription = appJson[kAppDesctiption] as? String
        let averageRating = appJson[kAverageRating] as? Float
        let averageRatingForCurrentVersion = appJson[kAverageRatingForCurrentVersion] as? Float
        let size = (appJson[kSize] as? NSString)?.integerValue
        let iconUrl = appJson[kImageUrl] as? String
        let screenshotUrls = appJson[kScreenshotsArray] as? [String]
        
        let app = iTunesApp(appName: appName,
                            appUrl: appUrl,
                            company: company,
                            companyUrl: companyUrl,
                            appDescription: appDescription,
                            averageRating: averageRating,
                            averageRatingForCurrentVersion: averageRatingForCurrentVersion,
                            size: size,
                            iconUrl: iconUrl,
                            screenshotUrls: screenshotUrls)
        
        return app
    }
}
