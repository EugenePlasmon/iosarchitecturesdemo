//
//  iTunesApp.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit


class iTunesApp {
    
    public typealias Bytes = Int
    
    public var appName: String?
    public var appUrl: String?
    public var company: String?
    public var companyUrl: String?
    public var appDescription: String?
    public var averageRating: Float?
    public var averageRatingForCurrentVersion: Float?
    public var size: Bytes?
    public var iconUrl: String?
    public var screenshotUrls: [String]?
    
    public var icon: UIImage? = nil
    
    init(appName: String?,
         appUrl: String?,
         company: String?,
         companyUrl: String?,
         appDescription: String?,
         averageRating: Float?,
         averageRatingForCurrentVersion: Float?,
         size: Bytes?,
         iconUrl: String?,
         screenshotUrls: [String]?) {
        self.appName = appName
        self.appUrl = appUrl
        self.company = company
        self.companyUrl = companyUrl
        self.appDescription = appDescription
        self.averageRating = averageRating
        self.averageRatingForCurrentVersion = averageRatingForCurrentVersion
        self.size = size
        self.iconUrl = iconUrl
        self.screenshotUrls = screenshotUrls
    }
}
