//
//  iTunesSearchManager.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import Alamofire


class iTunesSearchManager: NSObject {
    
    public typealias CompletionApps = (_ apps: [iTunesApp]?, _ error: Error?) -> Void
    public typealias CompletionSongs = (_ songs: [iTunesSong]?, _ error: Error?) -> Void
    
    private let networkManager = NetworkManager()
    private let appsParser = iTunesAppsParser()
    private let songsParser = iTunesSongsParser()
    
    private let baseUrl = "https://itunes.apple.com/search"
    private let defaultRegionCode = "RU"
    
    private enum MediaType: String {
        case apps = "software"
        case music = "music"
    }
    
    public func getApps(forQuery query: String,
                        completion: CompletionApps?) {
        var regionCode = Locale.current.regionCode
        
        if regionCode == nil {
            regionCode = defaultRegionCode
        }
        
        var parameters: Parameters = [:]
        parameters["term"] = query
        parameters["country"] = regionCode
        parameters["media"] = MediaType.apps.rawValue
        
        let request = WebRequest(method: .get,
                                 url: baseUrl,
                                 parameters: parameters)
        
        networkManager.executeRequest(request) { (response, error) in
            
            if error != nil {
                if let completion = completion {
                    completion(nil, error)
                }
                
                return
            }
            
            print("\(response?.description ?? "empty response")")
            
            let apps = self.appsParser.parseResponse(response!)
            
            if let completion = completion {
                completion(apps, nil)
            }
        }
    }
    
    public func getSongs(forQuery query: String,
                         completion: CompletionSongs?) {
        var regionCode = Locale.current.regionCode
        
        if regionCode == nil {
            regionCode = defaultRegionCode
        }
        
        var parameters: Parameters = [:]
        parameters["term"] = query
        parameters["country"] = regionCode
        parameters["media"] = MediaType.music.rawValue
        
        let request = WebRequest(method: .get,
                                 url: baseUrl,
                                 parameters: parameters)
        
        networkManager.executeRequest(request) { (response, error) in
            
            if error != nil {
                if let completion = completion {
                    completion(nil, error)
                }
                
                return
            }
            
            print("\(response?.description ?? "empty response")")
            
            let songs = self.songsParser.parseResponse(response!)
            
            if let completion = completion {
                completion(songs, nil)
            }
        }
    }
}
