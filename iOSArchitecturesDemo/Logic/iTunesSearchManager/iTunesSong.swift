//
//  iTunesSong.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit


class iTunesSong {
    
    public var trackName: String?
    public var artistName: String?
    public var collectionName: String?
    public var artwork: String?
    
    public var icon: UIImage? = nil
    
    init(trackName: String?,
         artistName: String?,
         collectionName: String?,
         artwork: String?) {
        self.trackName = trackName
        self.artistName = artistName
        self.collectionName = collectionName
        self.artwork = artwork
    }
}
