//
//  iTunesSongsParser.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import Foundation


class iTunesSongsParser: NSObject {
    
    private typealias SongDictionary = [String: Any]
    
    private let kTrackName = "trackName"
    private let kArtistName = "artistName"
    private let kCollectionName = "collectionName"
    private let kArtwork = "artworkUrl100"
    
    public func parseResponse(_ response: Response) -> [iTunesSong] {
        let results = response["results"] as! [SongDictionary]
        
        var songs = [iTunesSong]()
        
        for songJson in results {
            let song = parseSongJson(songJson)
            songs.append(song)
        }
        
        return songs
    }
    
    private func parseSongJson(_ songJson: SongDictionary) -> iTunesSong {
        let trackName = songJson[kTrackName] as? String
        let artistName = songJson[kArtistName] as? String
        let collectionName = songJson[kCollectionName] as? String
        let artwork = songJson[kArtwork] as? String
        
        let song = iTunesSong(trackName: trackName,
                              artistName: artistName,
                              collectionName: collectionName,
                              artwork: artwork)
        
        return song
    }
}
