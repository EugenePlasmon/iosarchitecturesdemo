//
//  AppStartConfigurator.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import UIKit

class AppStartManager {
    
    var window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let rootVC = SearchViewController()
        rootVC.navigationItem.title = "Search via iTunes"
        
        let navVC = UINavigationController()
        navVC.navigationBar.barTintColor = UIColor.iviVarna
        navVC.navigationBar.isTranslucent = false
        navVC.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navVC.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        navVC.viewControllers = [rootVC]
        
        window.rootViewController = navVC
        window.makeKeyAndVisible()
    }
}
