//
//  NetworkManager.swift
//  iOSArchitecturesDemo
//
//  Created by ekireev on 19.02.2018.
//  Copyright © 2018 ekireev. All rights reserved.
//

import Foundation
import Alamofire


typealias Response = [String: Any]
typealias Completion = (_ response: Response?, _ error: Error?) -> Void


class NetworkManager: NSObject {
    
    public func executeRequest(_ request: WebRequest,
                               completion: Completion?) {
        print("executing request \(request)")
        
        weak var weakSelf = self
        
        Alamofire.request(request.url,
                          method: request.method,
                          parameters: request.parameters).validate().responseJSON { response in
                            switch response.result {
                            case .success(let json):
                                guard completion != nil else { break }
                                completion!(json as? [String : Any], nil)
                                
                            case .failure(let error):
                                weakSelf?.logError(error, request: request)
                                guard completion != nil else { break }
                                completion!(nil, error)
                            }
        }
    }
    
    private func logError(_ error: Error, request: WebRequest) {
        print("Error while executing request \(request.url), error: \(error.localizedDescription)")
    }
}
